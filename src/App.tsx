import React, { FC } from 'react';
import './App.less';

import Navbar from './Components/Navbar';

const App: FC = () => (
  <div className="App">
    <Navbar/>
  </div>
);

export default App;