
import { Carousel, Row, Col, Image, } from 'antd';
import Banner01 from '../../assets/images/banner-01.png';
import Banner02 from '../../assets/images/banner-02.png';
import Banner03 from '../../assets/images/banner-03.png';

function CarouselComp() {
  return (
    <div style={{marginTop: 30, paddingBottom: 10}}>
      {/* <Carousel arrows nextArrow={<ArrowRightOutlined />} prevArrow={<ArrowLeftOutlined />} > */}
      <Carousel arrows={true} autoplay>

        <Row justify="center">
          <Col>
          <Image src={Banner01} alt="Banner01" preview={false} className="bannerimg"/>
          </Col>
        </Row>
        <Row justify="center">
          <Col>
          <Image src={Banner02} alt="Banner02" preview={false} className="bannerimg"/>
          </Col>
        </Row>
        <Row justify="center">
          <Col>
          <Image src={Banner03} alt="Banner03" preview={false} className="bannerimg"/>
          </Col>
        </Row>
     
      </Carousel>
    </div>
  )
}

export default CarouselComp
