import React from 'react';
import bgLanding from '../../assets/images/bg_landing_page.svg';

import { Image } from 'antd';

function LandingComp() {
  return (
    <div>
      <Image style={{padding: 0, margin: "0px 0px 70px 0px", }}
      src={bgLanding}
      preview={false}
      id="imglanding"
    />
    </div>
  )
}

export default LandingComp
