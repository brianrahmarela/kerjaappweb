import React from 'react';
import ProductExplore from '../../assets/images/productexplore.svg';
import { Typography, Image, Row, Col } from 'antd';

const { Title, Text } = Typography;

function ProductExpComp() {
  return (
    <div >
      <div className="containerImgExp">
        <Image
          src={ProductExplore}
          preview={false}
          id="productexplore"
        />
        <div className="rowproductExpComp">
          <Title className="titleproductexp">Product Explanation</Title>
          <Row justify="center">
            <div className="stroketitle"></div>
          </Row>
          <Row justify="center" className="textproductexp">
            <Col span={17}>
              <Text >
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
              </Text>
            </Col>
          </Row>
          <Row justify="center" className="textproductexp">
            <Col span={17}>
              <Text>
                Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie.
              </Text>
            </Col>
          </Row>
        </div>
      </div>


    </div>
  )
}

export default ProductExpComp
