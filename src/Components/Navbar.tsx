import React from 'react';

// assets
import LogoKerjaApp from '../assets/icons/logo_kerjaapp.svg';
import { Layout, Menu, Avatar } from 'antd';
import { Route, Switch, Link } from 'react-router-dom';
//komponen
const Home = React.lazy(() => import('../Pages/Home'));
const Login = React.lazy(() => import('../Pages/Login'));

const { Header, Content, Footer } = Layout;

const Navbar = () => {
  return (
    <div>
      <Layout>
        <Header style={{ position: 'fixed', zIndex: 1, width: '100%', padding: 0, margin: 0 }}>
          <Menu theme="light" mode="horizontal" defaultSelectedKeys={['1']}>
            <Menu.Item key="1"><Link to="/"><Avatar shape="square" size="large" icon={<img src={LogoKerjaApp} alt="logokerjaapp" />} className="logo" />
            </Link></Menu.Item>
            <Menu.Item key="2"><Link to="/login">Login</Link></Menu.Item>
            <Menu.Item key="3"><Link to="/signup">Sign Up</Link></Menu.Item>
            <Menu.Item key="4">nav 4</Menu.Item>
            <Menu.Item key="5">nav 5</Menu.Item>
          </Menu>
        </Header>
        <Content className="site-layout" style={{ padding: '0', marginTop: 64 }}>
          <div className="site-layout-background" style={{ margin: 0, padding: 0, minHeight: 380, }}>
            <React.Suspense fallback={<div>Loading...</div>}>
              <Switch>
                <Route exact path='/' component={() => <Home />} />
                <Route path='/login' component={() => <Login />} />
                <Route path='/signup' component={() => <h1>Sign Up</h1>} />
              </Switch>
            </React.Suspense>
          </div>
        </Content>
        <Footer style={{ textAlign: 'center', margin: 0, padding: 20, backgroundColor: '#00B9FF' }}>Ant Design ©2018 Created by Ant UED</Footer>
      </Layout>
    </div>
  )
}

export default Navbar
