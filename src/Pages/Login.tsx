
import { Carousel } from 'antd';

const items = [
  {
    key: '1',
    title: 'konten 1',
    content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s'
  },
  {
    key: '2',
    title: 'konten 2',
    content: 'when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries'
  },
  {
    key: '3',
    title: 'konten 3',
    content: 'Contrary to popular belief, Lorem Ipsum is not simply random text,'
  },

]

function Login() {
  return (
    <div>
      <Carousel >
        {items.map((item) => {
          return (
              <div key={item.key}>
                <h1>
                {item.content}
                </h1>
              </div>
          )
        })}
      </Carousel>
    </div>
  )
}

export default Login
