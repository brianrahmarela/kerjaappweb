
import LandingComp from '../Components/Home/LandingComp';
import CarouselComp from '../Components/Home/CarouselComp';
import ProductExpComp from '../Components/Home/ProductExpComp';
import { Typography, Row, Col, Space, Button, Image } from 'antd';
import IlusPeople from '../assets/images/ilus_people.svg';

const { Title, Text } = Typography;

function Home() {

  return (
    <>
    <div>
      <LandingComp />
      <Row className="rowtextlanding">
        <Col span={24}>
          <Title id="titlelanding">Selamat</Title>
        </Col>
        <Col span={24}>
          <Title id="titlelanding2">Datang</Title>
        </Col>
        <Row style={{ marginTop: 20 }}>
          <Space size={24.3} direction="vertical">
            <Col xs={17} sm={13} lg={11} span={9}>
              <Text id="subtitlelanding">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </Text>
            </Col>
            <Col>
              <div className="site-button-ghost-wrapper">
                <Button size={'large'} type="primary" ghost>
                  Mulai
                </Button>
              </div>
            </Col>
          </Space>
        </Row>
      </Row>
      <Row className="rowiluspeoplelanding">
        <Image style={{ padding: 0, margin: 0, }}
          src={IlusPeople}
          preview={false}
          id="iluspeople"
          height={542.84}
        />
      </Row>
    </div>
    <div>
    <CarouselComp/>
    </div>
    <ProductExpComp/>
    </>
  )
}

export default Home

