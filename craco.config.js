const CracoLessPlugin = require('craco-less');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: { 
              '@primary-color': '#2C9BE6',
              "@font-family": "'Poppins','Open Sans', sans-serif",
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};